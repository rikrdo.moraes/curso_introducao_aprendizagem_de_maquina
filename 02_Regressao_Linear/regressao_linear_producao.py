import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression



def carregar_modelo_baseado_coeficientes():
    regressao_producao = LinearRegression()
    coeficientes_lidos_producao = pd.read_csv("modelo_regressao_linear_producao.csv")
    regressao_producao.coef_ = np.array([float(coeficientes_lidos_producao['assess']),float(coeficientes_lidos_producao['bdrms']), float(coeficientes_lidos_producao['lotsize']), float(coeficientes_lidos_producao['sqrft']), float(coeficientes_lidos_producao['colonial'])])
    regressao_producao.intercept_=coeficientes_lidos_producao['intercept_']
    regressao_producao.coeficientes_lidos_producao = coeficientes_lidos_producao
    return regressao_producao

def ler_dados_usuario():
    assess=float(input('Informe assess ')) 
    bdrms=float(input('Informe bdrms '))
    lotsize=float(input('Informe lotsize ')) 
    sqrft=float(input('Informe sqrft '))
    colonial=float(input('Informe colonial '))
    valores_passados_pelo_cliente=np.array([assess,bdrms,lotsize,sqrft,colonial])

    return valores_passados_pelo_cliente

def predicao(valores_passados_pelo_cliente, regressao_producao):
    predicao_producao_com_dados_cliente = regressao_producao.predict(valores_passados_pelo_cliente.reshape(1,-1))
    erro=float(regressao_producao.coeficientes_lidos_producao['MAE'])
    print('\nEstimado usuário a nossa análise chegou ao seguintes valores considerando um erro absoluto médio de {}'.format(erro))
    print('\nO valor mínimo que encontramos foi {}, o valor aproximado da predição é {} e o valor máximo que pode chegar é {}'.format(float(predicao_producao_com_dados_cliente-erro), float(predicao_producao_com_dados_cliente), float(predicao_producao_com_dados_cliente+erro)))

dados_lidos=ler_dados_usuario()

modelo_regressao_linear=carregar_modelo_baseado_coeficientes()

predicao(dados_lidos, modelo_regressao_linear)