import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics

def carregar_modelo():
    USAhousing = pd.read_csv('USA_Housing.csv')
    X = USAhousing[['Avg. Area Income', 'Avg. Area House Age', 'Avg. Area Number of Rooms',
               'Avg. Area Number of Bedrooms', 'Area Population']]
    y = USAhousing['Price']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=101)
    lm = LinearRegression()
    predicoes = lm.fit(X_train,y_train)
    predicoes = lm.predict(X_test)
    mae = metrics.mean_absolute_error(y_test, predicoes)
    lm.mae = mae
    print('modelo carregado')
    return lm

def predicao(valores_passados_pelo_cliente, regressao_linear):
    res_predicao = regressao_linear.predict(valores_passados_pelo_cliente.reshape(1,-1))
    erro=float(regressao_linear.mae)
    return (res_predicao, erro)
