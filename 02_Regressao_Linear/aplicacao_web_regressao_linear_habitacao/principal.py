from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np

from regressao_linear import carregar_modelo
from regressao_linear import predicao

def predicao_regressao_linear():
  valores_passados_pelo_cliente=np.array([float(area_income.value), float(house_age.value), float(number_rooms.value),float(number_bedrooms.value), float(area_population.value)])
  (res_predicao, erro) = predicao(valores_passados_pelo_cliente, modelo_regressao_linear)

  try:
    resultado = '<br>Estimado usuário a nossa análise chegou ao seguintes valores considerando um erro absoluto médio de {}'.format(erro)    
    resultado = resultado + '<br>O valor mínimo que encontramos foi {}, o valor aproximado da predição é {} e o valor máximo que pode chegar é {}'.format(float(res_predicao-erro), float(res_predicao), float(res_predicao+erro))

    div_widget.text = resultado

  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_regressao_linear=carregar_modelo()

area_income = TextInput(title="Média da renda dos residentes", value="")
house_age = TextInput(title="Idade da casa.", value="")
number_rooms = TextInput(title="Número de ambientes", value="")
number_bedrooms = TextInput(title="Número de quartos", value="")
area_population = TextInput(title="A população da cidade", value="")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Predição", button_type="success")
button_widget.on_click(predicao_regressao_linear)  

controls = widgetbox([area_income, house_age, number_rooms, number_bedrooms, area_population, button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)
curdoc().title = "Regressão Linear"    
