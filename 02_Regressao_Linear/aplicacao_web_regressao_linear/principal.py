from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np

from regressao_linear import carregar_modelo_baseado_coeficientes
from regressao_linear import predicao

def predicao_regressao_linear():
  valores_passados_pelo_cliente=np.array([float(assess.value),float(bdrms.value),float(lotsize.value),float(sqrft.value),float(colonial.value)])
  (res_predicao, erro) = predicao(valores_passados_pelo_cliente, modelo_regressao_linear)

  try:
    resultado = '<br>Estimado usuário a nossa análise chegou ao seguintes valores considerando um erro absoluto médio de {}'.format(erro)    
    resultado = resultado + '<br>O valor mínimo que encontramos foi {}, o valor aproximado da predição é {} e o valor máximo que pode chegar é {}'.format(float(res_predicao-erro), float(res_predicao), float(res_predicao+erro))

    div_widget.text = resultado

  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_regressao_linear=carregar_modelo_baseado_coeficientes()

assess = TextInput(title="Informe assess", value="")
bdrms = TextInput(title="Informe bdrms", value="")
lotsize = TextInput(title="Informe lotsize", value="")
sqrft = TextInput(title="Informe sqrft", value="")
colonial = TextInput(title="Informe colonial", value="")
teste = TextInput(title="Informe colonial", value="")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Análise", button_type="success")
button_widget.on_click(predicao_regressao_linear)  

controls = widgetbox([assess, bdrms, lotsize, sqrft, colonial, button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)
curdoc().title = "Regressão Linear"    

