import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression

def carregar_modelo_baseado_coeficientes():
    regressao_producao = LinearRegression()
    coeficientes_lidos_producao = pd.read_csv("modelo_regressao_linear_producao.csv")
    regressao_producao.coef_ = np.array([float(coeficientes_lidos_producao['assess']),float(coeficientes_lidos_producao['bdrms']), float(coeficientes_lidos_producao['lotsize']), float(coeficientes_lidos_producao['sqrft']), float(coeficientes_lidos_producao['colonial'])])
    regressao_producao.intercept_=coeficientes_lidos_producao['intercept_']
    regressao_producao.coeficientes_lidos_producao = coeficientes_lidos_producao
    return regressao_producao

def predicao(valores_passados_pelo_cliente, regressao_producao):
    res_predicao = regressao_producao.predict(valores_passados_pelo_cliente.reshape(1,-1))
    erro=float(regressao_producao.coeficientes_lidos_producao['MAE'])
    return (res_predicao, erro)
