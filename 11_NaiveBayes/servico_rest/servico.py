from flask import Flask
import pandas as pd
import numpy as np
from nb import carregar_modelo
from nb import predicao

app = Flask(__name__)

modelo_nb=carregar_modelo()

@app.route("/predicao/<tweet>")
def predict(tweet=""):
    valores_passados_pelo_cliente=np.array([tweet])
    resultado_predicao = predicao(valores_passados_pelo_cliente, modelo_nb)

    resultado ='<br>A mensagem no tweet tem teor {}. Seria interessante a equipe de marketing avaliar'.format(resultado_predicao)

    return resultado

if __name__ == "__main__":    
    app.run(debug=True)

#curl  http://127.0.0.1:5000/predicao/"governo ruim"
