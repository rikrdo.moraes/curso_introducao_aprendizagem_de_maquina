import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict

def carregar_modelo():
    dataset = pd.read_csv('Tweets_Mg.csv',encoding='utf-8')
    tweets = dataset['Text'].values
    classes = dataset['Classificacao'].values
    vectorizer = CountVectorizer(analyzer="word")
    freq_tweets = vectorizer.fit_transform(tweets)
    modelo = MultinomialNB()
    modelo.fit(freq_tweets,classes)
    modelo.vectorizer = vectorizer
    return modelo

def predicao(valores_passados_pelo_cliente, modelo):
    freq = modelo.vectorizer.transform(valores_passados_pelo_cliente)
    resposta_predicao = modelo.predict(freq)
    return (resposta_predicao)
