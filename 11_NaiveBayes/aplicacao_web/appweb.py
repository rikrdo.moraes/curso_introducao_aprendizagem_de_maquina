from bokeh.layouts import row, column, widgetbox
from bokeh.models import TextInput, Button,  Div
from bokeh.plotting import curdoc
import pandas as pd
import numpy as np
from nb import carregar_modelo
from nb import predicao

def predicao_nb():

  valores_passados_pelo_cliente=np.array([Tweets_Mg.value])
  resultado_predicao = predicao(valores_passados_pelo_cliente, modelo_nb)

  try:
    resultado ='<br>A mensagem no tweet tem teor {}. Seria interessante a equipe de marketing avaliar'.format(resultado_predicao)

    div_widget.text = resultado
  except KeyboardInterrupt:
    return 
  except ValueError as e:
    div_widget.text = "<h2>Um erro ocorreu: {}</h2>".format(str(e))
  except Exception as e:
    errMsg = "<h2>Ocorreu um erro inesperado com a mensagem de erro: <br>"
    div_widget.text = errMsg

modelo_nb=carregar_modelo()


Tweets_Mg = TextInput(title='Mensagem Tweet ', value="")

div_widget = Div(text="", width=400, height=100)

button_widget = Button(label="Análise de sentimento do tweet", button_type="success")
button_widget.on_click(predicao_nb)  

controls = widgetbox([
Tweets_Mg, 
button_widget], width=200)

layout = row(column(controls), column(div_widget))

curdoc().add_root(layout)

curdoc().title = "Naive Bayes - Análise de Sentimento"    
