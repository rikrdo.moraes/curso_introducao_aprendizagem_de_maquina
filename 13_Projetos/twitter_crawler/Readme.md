

* For csv
   ```sh
    scrapy crawl twittercrawler -a filename=myhashtags.csv -o mydata.csv

   ```
   
* For JSON
   ```sh
    scrapy crawl twittercrawler -a filename=myhashtags.csv -o mydata.json

   ```


## Data Columns
* username
* full_name
* twitter_url
* tweet_text
* tweet_time
* number_of_likes
* no_of_retweets
* no_of_replies
* mentions
* no_of_mentions
* hashtags
* no_of_hashtags
* call_to_action
* image_url


