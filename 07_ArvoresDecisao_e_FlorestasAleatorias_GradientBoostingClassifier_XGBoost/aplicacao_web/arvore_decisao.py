from sklearn.datasets import load_breast_cancer
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier

def carregar_modelo_aprendizado():
    df = pd.read_csv('kyphosis.csv')
    X = df.drop('Kyphosis',axis=1)
    y = df['Kyphosis']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)
    rfc = RandomForestClassifier(n_estimators=100)
    rfc.fit(X_train, y_train)    

    return rfc

def predicao(valores_passados_pelo_cliente, modelo_aprendizado):
    resposta_predicao = modelo_aprendizado.predict(valores_passados_pelo_cliente.reshape(1,-1))
    return (resposta_predicao)
